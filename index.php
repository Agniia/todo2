<?php
    session_start ();
    header('Content-Type: text/html; charset=utf-8');
    error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
    ini_set('display_errors', 1);
    require 'vendor/autoload.php';
    function autoloader($className)
    {   
       $file = str_replace('\\',DIRECTORY_SEPARATOR,$className);
       $fileName = 'classes'.DIRECTORY_SEPARATOR . $file . '.php'; 
       if(file_exists($fileName))
        {
    		include $fileName;
        }
    }
    spl_autoload_register('autoloader');
    
    class Utile
    {
        static $utile= null;
        public $router;
        public $dbInstance;
        
        function __construct()
    	{
    	}
        
        public static function get()
        {
            if (!self::$utile) {
                self::$utile = new Utile();
            }
            return self::$utile;
        }
        
        public function config()
        {
            $config = include 'config.php';
            return $config;
        }
        
        public function render($template, $params = [])
        {
            $loader = new Twig_Loader_Filesystem('views');
            $twig = new Twig_Environment($loader);       
            $params['dir'] = '/u/anshukova/todo2/';  
            $params['session'] = $_SESSION['user_id']; 
            echo $twig->render($template, $params); 
        }

    }
    $utile = Utile::get();
    $utile->router = new \Router\Router();
    $config = $utile->config();
    $utile->dbInstance = new \DBManager\DBManager($config['host'], 
            $config['user'], $config['pass'], $config['db']);
    $utile->router->getControllerAction($utile->dbInstance);