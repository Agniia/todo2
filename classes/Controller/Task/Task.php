<?php
namespace Controller\Task; 

class Task
{	
	protected $taskModel;
	
	function __construct($dbInstance)
	{
		$this->taskModel = new \Model\Task\Task($dbInstance);
	}
	
	public function add()
	{
		if(!empty($_POST['description']) && isset($_SESSION['user_id']))
	     {
	        $user_id = (int)($_SESSION['user_id']);
	        $assigned_user_id = (int)($_SESSION['user_id']);
	        $description = strip_tags($_POST['description']);
	        $is_done = 0;
	        $date_added = date('d-m-Y');
	        $task = $this->taskModel->add($user_id, $assigned_user_id,$description,$is_done,$date_added); 	     
			$this->allyour();
	    }
	}
	public function updateStatus()
	{
		 if(isset($_POST['status']) && isset($_POST['task_id']) && isset($_SESSION['user_id']))
	     {
	        $task_id = (int)$_POST['task_id'];
	        $status = (int)$_POST['status'];
	        $task = $this->taskModel->changeTaskStatus($task_id, $status); 	     
	    }    
	    $this->allforyou();
	}
	
	public function updateResponsibleAndStatus()
	{
		if(isset($_POST['assigned_user_id']) && isset($_POST['task_id']) && isset($_POST['status']) && isset($_SESSION['user_id']))
	     {
	        $task_id = (int)($_POST['task_id']);
	        $assigned_user_id = (int)($_POST['assigned_user_id']);
	        $status = (int)$_POST['status'];
	        $task = $this->taskModel->changeTaskResponsibleAndStatus($task_id, $assigned_user_id,$status); 	     
	    }
	    $this->allyour();
	}
	
	public function update()
	{
		 if(isset($_POST['assigned_user_id']) && isset($_POST['task_id']) && isset($_SESSION['user_id']))
	     {
	        $task_id = (int)($_POST['task_id']);
	        $assigned_user_id = (int)($_POST['assigned_user_id']);
	        $task = $this->taskModel->changeTaskResponsible($task_id, $assigned_user_id); 	     
			$this->all();
	    }
	    else if(isset($_POST['status']) && isset($_POST['task_id']) && isset($_SESSION['user_id']))
	     {
	        $task_id = $_POST['task_id'];
	        $status = $_POST['status'];
	        $task = $this->taskModel->changeTaskStatus($task_id, $status); 	     
			$this->all();
	    }    
	}
	
	public function allforyou()
	{
		if(isset($_GET['id'])){
			$id_user = (int)$_GET['id'];
			$all_users = $this->taskModel->allUsers();
			$tasks = $this->taskModel->allforyou($id_user);
			\Utile::get()->render('update.html', ['header' => 'Все задачи, поставленные Вам','tasks' => $tasks,  'all_users' => $all_users, 'can_add_task' =>'0']);	
		}
	}
			
	public function allyour()
	{
		if(isset($_GET['id'])){
			$id_user = (int)$_GET['id'];
			$all_users = $this->taskModel->allUsers();
			$tasks = $this->taskModel->allyour($id_user);
			\Utile::get()->render('update.html', ['header' => 'Все задачи, поставленные Вами','tasks' => $tasks, 'all_users' => $all_users, 'can_add_task' =>'1']);	
		}
	}			
			
	public function all()
	{
		$tasks = $this->taskModel->all();
		\Utile::get()->render('all.html', array('header' => 'Все задачи','tasks' => $tasks,'can_add_task' =>'0'));	
	}
	
}