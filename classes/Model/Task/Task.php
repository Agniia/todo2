<?php
namespace Model\Task; 

class Task
{	
	protected $dbHandler;
	
	function __construct($dbInstance)
	{
		$this->dbHandler = $dbInstance->getDbHandler();
	}
		
	public function add($user_id, $assigned_user_id,$description,$is_done,$date_added)
	{
		try {
			$stmt = $this->dbHandler->prepare("INSERT INTO tasks (user_id, assigned_user_id, description, is_done, date_added) VALUES (:user_id, :assigned_user_id, :description, :is_done, :date_added)");
			$stmt->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
			$stmt->bindParam(':assigned_user_id', $assigned_user_id, \PDO::PARAM_INT);
			$stmt->bindParam(':description', $description, \PDO::PARAM_STR);
			$stmt->bindParam(':is_done', $is_done, \PDO::PARAM_INT);
			$stmt->bindParam(':date_added', $date_added, \PDO::PARAM_STR);
		    $res = $stmt->execute();
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function changeTaskResponsible($task_id, $assigned_usr_id) 
	{
		$id = (int)$task_id;
		$assigned_user_id = (int)$assigned_usr_id;
		try {
			$stmt = $this->dbHandler->prepare("UPDATE tasks SET assigned_user_id = :assigned_user_id WHERE id = :id");
			$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
			$stmt->bindParam(':assigned_user_id', $assigned_user_id, \PDO::PARAM_INT);
		    $res = $stmt->execute();
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function changeTaskStatus($task_id, $status) 
	{
		$id = (int)$task_id;
		$is_done = (int)$status;
		try {
			$stmt = $this->dbHandler->prepare("UPDATE tasks SET is_done = :is_done WHERE id = :id");
			$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
			$stmt->bindParam(':is_done', $is_done, \PDO::PARAM_INT);
		    $res = $stmt->execute();
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
  }	
  
  	public function changeTaskResponsibleAndStatus($task_id, $assigned_usr_id,$status)
  	{     
		$id = (int)$task_id;
		$assigned_user_id = (int)$assigned_usr_id;
		$is_done = (int)$status;
		try {
			$stmt = $this->dbHandler->prepare("UPDATE tasks SET is_done = :is_done,  assigned_user_id = :assigned_user_id WHERE id = :id");
			$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
			$stmt->bindParam(':is_done', $is_done, \PDO::PARAM_INT);
			$stmt->bindParam(':assigned_user_id', $assigned_user_id, \PDO::PARAM_INT);
		    $res = $stmt->execute();
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}

	public function allUsers()
	{
		$sth = $this->dbHandler->prepare('SELECT DISTINCT tasks.user_id, owners.login as owner_login FROM tasks JOIN users as owners ON tasks.user_id = owners.id');
	    $sth->execute();
	    return $sth->fetchAll();
	}
	
	public function all()
	{
		$sth = $this->dbHandler->prepare( 'SELECT DISTINCT tasks.id as task_id, tasks.is_done as status, tasks.description as task_description, tasks.date_added, tasks.user_id, tasks.assigned_user_id, owners.login as owner_login,  responsibles.login as responsible_login,  responsibles.id as responsible_id  FROM tasks 
	   JOIN users as owners ON tasks.user_id = owners.id 
	   JOIN users as responsibles ON tasks.assigned_user_id = responsibles.id');
	    $sth->execute();	    
	    return $sth->fetchAll();
	}
	
	public function allUserOwnerOrRespons($id_user)
	{
		$assigned_user_id = (int)$id_user;		
		$sth = $this->dbHandler->prepare('SELECT DISTINCT tasks.id as task_id, tasks.is_done as status, tasks.description as task_description, tasks.date_added, tasks.user_id, tasks.assigned_user_id, owners.login as owner_login,  responsibles.login as responsible_login,  responsibles.id as responsible_id  FROM tasks 
		JOIN users as owners ON tasks.user_id = owners.id 
		JOIN users as responsibles ON tasks.assigned_user_id = responsibles.id
		WHERE tasks.assigned_user_id = '.$id_user.' OR tasks.user_id = '.$id_user);
	    $sth->execute();	    
	    return $sth->fetchAll();
	}
	
	public function allyour($id_user)
	{
		$assigned_user_id = (int)$id_user;
		$sth = $this->dbHandler->prepare('SELECT DISTINCT tasks.id as task_id, tasks.is_done as status, tasks.description as task_description, tasks.date_added, tasks.user_id, tasks.assigned_user_id, owners.login as owner_login, owners.id as owner_id, responsibles.login as responsible_login,  responsibles.id as responsible_id  FROM tasks 
		JOIN users as owners ON tasks.user_id = owners.id 
		JOIN users as responsibles ON tasks.assigned_user_id = responsibles.id
		WHERE tasks.user_id = '.$id_user);
	    $sth->execute();	    
	    return $sth->fetchAll();
	}
	
	public function allforyou($id_user)
	{
		$sth = $this->dbHandler->prepare('SELECT DISTINCT tasks.id as task_id, tasks.is_done as status, tasks.description as task_description, tasks.date_added, tasks.user_id, tasks.assigned_user_id, owners.login as owner_login, owners.id as owner_id, responsibles.login as responsible_login,  responsibles.id as responsible_id  FROM tasks 
		JOIN users as owners ON tasks.user_id = owners.id 
		JOIN users as responsibles ON tasks.assigned_user_id = responsibles.id
		WHERE tasks.user_id != '.$id_user.' AND tasks.assigned_user_id = '.$id_user);
	    $sth->execute();	    
	    return $sth->fetchAll();
	}
}