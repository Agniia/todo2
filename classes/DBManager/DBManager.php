<?php
namespace DBManager; 

class DBManager
{	
	protected $host;
	protected $user;
	protected $pass;
	protected $db;
	protected $dbHandler; 
	
	function __construct($host, $user, $pass, $db)
	{
		$this->host = $host;
		$this->user =  $user;
		$this->pass = $pass;
		$this->db = $db;
		$this->doConnect();
	}
	
	protected function doConnect()
	{
		try
		{
			$this->dbHandler = new \PDO('mysql:host='.$this->host.';dbname='.$this->db, $this->user, $this->pass);
		 	$this->dbHandler->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
		 	$this->dbHandler->exec("set names utf8");
	 	}
	    catch (PDOException $e) 
	    {
			 print "Error!: " . $e->getMessage() . "<br/>";
			 die();
		}
	}
	
	public function getDbHandler()
	{
		$this->doConnect();
		return $this->dbHandler;
	}
}