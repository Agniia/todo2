<?php include 'template/header.php'; ?>
<h2><?php echo $header; ?></h2>
  <table class="tasks table table-dark">
    <tr>
         <td>Описание задачи</td>
        <td>Кто поставил задачу</td>
        <td>Ответственный</td>
        <td>Статус</td>
        <td>Дата добавления</td>
    </tr>
       <?php  foreach($tasks as $task):?>
            <tr>
            <td><?php echo $task['task_description']; ?></td>
            <td><?php echo $task['owner_login']; ?></td>
            <td><?php echo $task['responsible_login']; ?></td>
            <td><?php
                $status = ($task['status'] == 1) ? 'Выполнено' : 'Не выполено';
                echo $status; 
             ?></td>
            <td><?php echo $task['date_added']; ?></td>
            </tr>
        <?php endforeach; ?>
  </table>
 <?php  if($can_add_task == 1):?>
    <h2>Добавить задачу</h2>
     <form action="index.php?c=task&a=add&id=<?php echo $_SESSION['user_id']; ?>" method="post" id="new_task">
        <input type="text" name="description" placeholder="Описание задачи" required> <button class="btn btn-primary" type="submit"  form="new_task">Добавить задачу</button>
     </form>
 <?php endif; ?>